/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.uni.ni.prgramacion1.projectocarro;

/**
 *
 * @author Sistemas36
 */
public class Carro {

    /**
     * @param args the command line arguments
     */
      public static void main(String[] args) {
        Instancias Instancia1 = new Instancias("Nissan", 2016, "xTerreno");
      
       printInstancia(Instancia1);
       
        
    }
    
     public static void printInstancia (Instancias e){
         System.out.println("Atributos del carro:");
        System.out.println("Marca " + e.getMarca());
        System.out.println("Año " + e.getAño());
        System.out.println("Modelo " + e.getModelo());
     }
}


