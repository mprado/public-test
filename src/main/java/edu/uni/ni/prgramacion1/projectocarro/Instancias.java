/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.uni.ni.prgramacion1.projectocarro;


public class Instancias {
    private String marca;
    private int año;
    private String modelo;
    
 public Instancias() {
    }

public Instancias(String marca, int año, String modelo){
this.marca = marca;
this.año = año;
this.modelo = modelo;

}

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
}